<?php

namespace App\Http\Controllers\test;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class TestController extends Controller
{
    //
    public function save(Request $request) {
        $json = $request->input('json');
        if($json) {
            $response = Storage::disk('json')->put('save.json', $json);
            if($response) {
                return 'Downloaded to storage/json/save.json 
                <a href="/"><input type="button" value="Go Back"></a>
                <a target="_blank" href="/preview"><input type="button" value="Preview"></a>';
            } else {
                return 'Failed <a href="/">Go Back</a>';
            }
        }
        return 'error <a href="/">Go Back</a>';
    }
}
