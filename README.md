# Warehouse Report Design

## About
The project is using Laravel framework to build. The report is designed in table form. User can drag and drop
each td element in the table to design the report layout.

## Concept
The concept of the report design is saving the report layout design into data (json).  
The concept of the drag and drop is using html5 and js feature. --public/js/drag.js.  
The report layout output is based on the resources/view/saveToPrint.blade.php file.  
The data of report layout is saved in storage/json/save.json file.  

## Improvement
### For now, the layout must use table form.  
- Suggest to output the element using js nodeName (type).  
### User interface.  
- Button, cursor can be beautify.  
### The php code must be code separately.  
- Change to js  
