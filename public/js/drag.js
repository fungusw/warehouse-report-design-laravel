function load() {
    const draggables = document.querySelectorAll(".component");
    const containers = document.querySelectorAll(".container");
    draggables.forEach((component) => {
        component.setAttribute("draggable", true);
        component.addEventListener("dragstart", () => {
            component.classList.add("dragging");
        });

        component.addEventListener("dragend", () => {
            component.classList.remove("dragging");
        });
    });

    containers.forEach((droppableComponentContainer) => {
        droppableComponentContainer.addEventListener("dragover", (e) => {
            e.preventDefault();
            const afterElement = getDragAfterElement(
                droppableComponentContainer,
                e.clientY
            );
            const component = document.querySelector(".dragging");
            // console.log('c', droppableComponentContainer);
            // console.log('a', afterElement);
            // console.log('a', component);
            if (afterElement == null) {
                // console.log('a', droppableComponentContainer);
                // console.log('b', component);
                droppableComponentContainer.appendChild(component);
            } else {
                // console.log('a', droppableComponentContainer);
                // console.log('b', component);
                droppableComponentContainer.insertBefore(
                    component,
                    afterElement
                );
            }
        });
    });

    // const print = document.querySelectorAll('.print-component');
    // print.forEach(element => {
    //     element.style.display = 'none';
    // })
    // const drag = document.querySelectorAll('.drag-component');
    // drag.forEach(element => {
    //     element.style.display = 'table-row';
    // })

    const draggables2 = document.querySelectorAll(".component2");
    const containers2 = document.querySelectorAll(".container2");
    draggables2.forEach((component) => {
        component.setAttribute("draggable", true);
        component.addEventListener("dragstart", () => {
            component.classList.add("dragging2");
        });

        component.addEventListener("dragend", () => {
            component.classList.remove("dragging2");
        });
    });

    containers2.forEach((droppableComponentContainer) => {
        droppableComponentContainer.addEventListener("dragover", (e) => {
            e.preventDefault();
            const afterElement = getDragAfterElement2(
                droppableComponentContainer,
                e.clientX
            );
            const component = document.querySelector(".dragging2");
            // console.log('c', droppableComponentContainer);
            // console.log('a', afterElement);
            // console.log('a', component);
            if (afterElement == null) {
                // console.log('a', droppableComponentContainer);
                // console.log('b', component);
                droppableComponentContainer.appendChild(component);
            } else {
                // console.log('a', droppableComponentContainer);
                // console.log('b', component);
                droppableComponentContainer.insertBefore(
                    component,
                    afterElement
                );
            }
        });
    });
}

function clickFunction() {
    var modify = modifyHTMLtoJSON("main");
    document.getElementById('inputJSON').value = JSON.stringify(modify);
    document.getElementById('myForm').submit();
}

load();

function getDragAfterElement(droppableComponentContainer, y) {
    const draggableElements = [
        ...droppableComponentContainer.querySelectorAll(
            ".component:not(.dragging)"
        ),
    ];
    return draggableElements.reduce(
        (closest, child) => {
            const box = child.getBoundingClientRect();
            const offset = y - box.top - box.height / 2;
            if (offset < 0 && offset > closest.offset) {
                return {
                    offset: offset,
                    element: child,
                };
            } else {
                return closest;
            }
        }, {
            offset: Number.NEGATIVE_INFINITY,
        }
    ).element;
}

function getDragAfterElement2(droppableComponentContainer, y) {
    const draggableElements = [
        ...droppableComponentContainer.querySelectorAll(
            ".component2:not(.dragging2)"
        ),
    ];
    return draggableElements.reduce(
        (closest, child) => {
            const box = child.getBoundingClientRect();
            const offset = y - box.left - box.width / 2;
            console.log(offset);
            if (offset < 0 && offset > closest.offset) {
                return {
                    offset: offset,
                    element: child,
                };
            } else {
                return closest;
            }
        }, {
            offset: Number.NEGATIVE_INFINITY,
        }
    ).element;
}


function modifyHTMLtoJSON(main) {
    var mainDoc = document.getElementById(main);
    var jsonData = {};
    // mainDoc.children
    const tableChildren = [...mainDoc.children];
    var tableJSON = [];
    tableChildren.forEach((tableChild) => {
        const tbodyChildren = [...tableChild.children];
        var tbodyJSON = [];
        tbodyChildren.forEach((tbodyChild) => {
            const trChildren = [...tbodyChild.children];
            var trJSON = [];
            trChildren.forEach((trChild) => {
                const tdChildren = [...trChild.children];
                var tdJSON = [];
                tdChildren.forEach((tdChild) => {
                    if (tdChild.childElementCount == 0) {
                        var td = {
                            id: tdChild.id,
                            type: tdChild.nodeName,
                            class: tdChild.className,
                            style: tdChild.style.cssText,
                            content: tdChild.innerHTML.trim(),
                            width: tdChild.getAttribute('width'),
                            colspan: tdChild.getAttribute('colspan'),
                        };
                        // console.log(tdChild.getAttribute('width'));
                        tdJSON = [...tdJSON, td];
                        // console.log(tdJSON);
                    } else {
                        const divChildren = [...tdChild.children];
                        var divJSON = [];
                        divChildren.forEach((divChild) => {
                            var div = {
                                id: divChild.id,
                                type: divChild.nodeName,
                                class: divChild.className,
                                style: divChild.style.cssText,
                                content: divChild.innerHTML.trim(),
                            };
                            // console.log(div);
                            divJSON = [...divJSON, div];
                        });
                        var td = {
                            id: tdChild.id,
                            type: tdChild.nodeName,
                            class: tdChild.className,
                            style: tdChild.style.cssText,
                            content: divJSON,
                            width: tdChild.getAttribute('width'),
                            colspan: tdChild.getAttribute('colspan'),
                        };
                        tdJSON = [...tdJSON, td];
                    }
                });
                var tr = {
                    id: trChild.id,
                    type: trChild.nodeName,
                    class: trChild.className,
                    style: trChild.style.cssText,
                    content: tdJSON,
                };
                trJSON = [...trJSON, tr];
            });
            var tbody = {
                id: tbodyChild.id,
                type: tbodyChild.nodeName,
                class: tbodyChild.className,
                style: tbodyChild.style.cssText,
                content: trJSON,
            };
            tbodyJSON = [...tbodyJSON, tbody];
        });
        var table = {
            id: tableChild.id,
            type: tableChild.nodeName,
            class: tableChild.className,
            style: tableChild.style.cssText,
            content: tbodyJSON,
        };
        tableJSON = [...tableJSON, table];
    });
    jsonData = {
        table: tableJSON
    };
    return jsonData;
}
