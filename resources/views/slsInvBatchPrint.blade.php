<!-- top in mm unit -->
<?php 
$initTopAddress = 39; 
$initTopDocument = 49; 
$initTopDetails = 96; 
$initTopRinggit = 247; 
$initTopBarcode = 255; 
$initTopSignature = 262; 
$initTopTime = 33; 
$initTopDesc = 241; 
?>

<!-- px value in 1 mm unit -->
<?php $rowsPerPage = 9; ?>
<?php $ttlRows = count($docHdr->details); ?>
<?php $pageCount = $ttlRows % $rowsPerPage == 0 ? (int) ($ttlRows / $rowsPerPage) : (int) ($ttlRows / $rowsPerPage) + 1 ?>

@for ($pageNum = 1; $pageNum <= $pageCount; $pageNum++)
    <?php 
        //portrait
        $loopHeight = $pageHeight;
        //use request scope variable
        $pageLoop = request()->get('slsInvBatchPrint.pageLoop', 0);

        $topAddress = $initTopAddress + $loopHeight * $pageLoop;       
        $topDocument = $initTopDocument + $loopHeight * $pageLoop;
        $topDetails = $initTopDetails + $loopHeight * $pageLoop;
        $topRinggit = $initTopRinggit + $loopHeight * $pageLoop;
        $topBarcode = $initTopBarcode + $loopHeight * $pageLoop;
        $topSignature = $initTopSignature + $loopHeight * $pageLoop;
        $topTime = $initTopTime + $loopHeight * $pageLoop;
        $topDesc = $initTopDesc + $loopHeight * $pageLoop;

        $pageLoop++;
        request()->merge(array('slsInvBatchPrint.pageLoop' => $pageLoop));
    ?>
    
    <div>
        <!-- Bill To -->
        <table style="top:{{$topAddress}}mm;left:12mm;position:absolute;">
            <tr>
                <td>
                    <div style="width:63mm;height:5mm;font-size:0.9em;font-weight:normal;text-align:left;overflow:hidden;">
                    Bill To:
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div style="width:63mm;height:5mm;font-size:0.9em;font-weight:normal;text-align:left;overflow:hidden;">
                    {{$docHdr->debtor_code}}
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div style="width:123mm;height:5mm;font-size:0.9em;font-weight:normal;text-align:left;overflow:hidden;">
                    {{$docHdr->debtor_company_name_01}}
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div style="width:63mm;height:25mm;font-size:0.9em;font-weight:normal;text-align:left;overflow:hidden;">
                    {{$docHdr->debtor_unit_no}}
                    {{$docHdr->debtor_building_name}}<br />
                    {{$docHdr->debtor_street_name}}<br />
                    {{$docHdr->debtor_district_01}}<br />
                    {{$docHdr->debtor_district_02}}<br />
                    {{$docHdr->debtor_postcode}}<br />
                    {{$docHdr->debtor_state_name}}
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div style="width:63mm;height:5mm;font-size:0.9em;font-weight:normal;text-align:left;overflow:hidden;">
                    TEL : {{$docHdr->debtor_phone_01}}
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div style="width:63mm;height:5mm;font-size:0.9em;font-weight:normal;text-align:left;overflow:hidden;">
                    FAX : {{$docHdr->debtor_fax_01}}
                    </div>
                </td>
            </tr>
        </table>

        <!-- Deliver To -->
        <table style="top:{{$topAddress}}mm;left:85mm;position:absolute;">
            <tr>
                <td>
                    <div style="width:63mm;height:5mm;font-size:0.9em;font-weight:normal;text-align:left;overflow:hidden;">
                    Deliver To:
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div style="width:63mm;height:5mm;font-size:0.9em;font-weight:normal;text-align:left;overflow:hidden;">
                    
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div style="width:63mm;height:5mm;font-size:0.9em;font-weight:normal;text-align:left;overflow:hidden;">
                    
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div style="width:62mm;height:25mm;font-size:0.9em;font-weight:normal;text-align:left;overflow:hidden;">
                    {{$docHdr->delivery_point_unit_no}}
                    {{$docHdr->delivery_point_building_name}}<br />
                    {{$docHdr->delivery_point_street_name}}<br />
                    {{$docHdr->delivery_point_district_01}}<br />
                    {{$docHdr->delivery_point_district_02}}<br />
                    {{$docHdr->delivery_point_postcode}}<br />
                    {{$docHdr->delivery_point_state_name}}
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div style="width:63mm;height:5mm;font-size:0.9em;font-weight:normal;text-align:left;overflow:hidden;">
                    TEL : {{$docHdr->delivery_point_phone_01}}
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div style="width:63mm;height:5mm;font-size:0.9em;font-weight:normal;text-align:left;overflow:hidden;">
                    FAX : {{$docHdr->delivery_point_fax_01}}
                    </div>
                </td>
            </tr>
        </table>

        <!-- Picking/So No -->
        <table style="top:{{$topAddress}}mm;left:146mm;position:absolute;">
            <tr>
                <td>
                    <div style="width:16mm;height:4mm;font-size:0.7em;font-weight:bold;text-align:left;overflow:hidden;">
                    SO NO
                    </div>
                </td>
                <td>
                    <div style="width:39mm;height:4mm;font-size:0.7em;font-weight:normal;text-align:right;overflow:hidden;">
                    {{$docHdr->sls_ord_hdr_doc_code}}
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div style="width:16mm;height:4mm;font-size:0.7em;font-weight:bold;text-align:left;overflow:hidden;">
                    PICKING
                    </div>
                </td>
                <td>
                    <div style="width:39mm;height:4mm;font-size:0.7em;font-weight:normal;text-align:right;overflow:hidden;">
                    {{$docHdr->pick_list_hdr_doc_code}}
                    </div>
                </td>
            </tr>
        </table>

        <!-- Doc No/Date/Ref No -->
        <table style="top:{{$topDocument}}mm;left:146mm;position:absolute;">
            <tr>
                <td>
                    <div style="width:20mm;height:5mm;font-size:0.9em;font-weight:bold;text-align:left;overflow:hidden;">
                    Invoice No
                    </div>
                </td>
                <td>
                    <div style="width:35mm;height:5mm;font-size:0.9em;font-weight:normal;text-align:right;overflow:hidden;">
                    {{$docHdr->doc_code}}
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div style="width:20mm;height:5mm;font-size:0.9em;font-weight:bold;text-align:left;overflow:hidden;">
                    Date
                    </div>
                </td>
                <td>
                    <div style="width:35mm;height:5mm;font-size:0.9em;font-weight:normal;text-align:right;overflow:hidden;">
                    {{date("Y-m-d", strtotime($docHdr->doc_date))}}
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div style="width:20mm;height:5mm;font-size:0.9em;font-weight:bold;text-align:left;overflow:hidden;">
                    Ref Code
                    </div>
                </td>
                <td>
                    <div style="width:35mm;height:5mm;font-size:0.8em;font-weight:normal;text-align:right;overflow:hidden;">
                    {{$docHdr->ref_code_01}}
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div style="width:20mm;height:5mm;font-size:0.9em;font-weight:bold;text-align:left;overflow:hidden;">
                    Salesman
                    </div>
                </td>
                <td>
                    <div style="width:35mm;height:5mm;font-size:0.9em;font-weight:normal;text-align:right;overflow:hidden;">
                    {{$docHdr->salesman_code}}
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div style="width:20mm;height:5mm;font-size:0.9em;font-weight:bold;text-align:left;overflow:hidden;">
                    Terms
                    </div>
                </td>
                <td>
                    <div style="width:35mm;height:5mm;font-size:0.9em;font-weight:normal;text-align:right;overflow:hidden;">
                    {{$docHdr->credit_term_code}}
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div style="width:20mm;height:5mm;font-size:0.9em;font-weight:bold;text-align:left;overflow:hidden;">
                    Page
                    </div>
                </td>
                <td>
                    <div style="width:35mm;height:5mm;font-size:0.9em;font-weight:normal;text-align:right;overflow:hidden;">
                    {{$pageNum}} / {{$pageCount}}
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div style="width:20mm;height:5mm;font-size:0.9em;font-weight:bold;text-align:left;overflow:hidden;">
                    Collector
                    </div>
                </td>
                <td>
                    <div style="width:35mm;height:5mm;font-size:0.9em;font-weight:normal;text-align:right;overflow:hidden;">
                    {{$docHdr->ref_code_02}}
                    </div>
                </td>
            </tr>
        </table>

        <!-- Document Details -->
        <table style="top:{{$topDetails}}mm;table-layout:fixed;width:191mm;left:12mm;position:absolute;">
            <tr>
            <td>
            <!-- heading -->
            <table style="table-layout:fixed;width:191mm;border-bottom:1px solid black;display: table-header-group;">
                <tr style="font-size:0.9em;font-weight:bold;text-align:left;overflow:hidden;">
                    <th width="4%" style=""></th>
                    <th width="18%" style="">Item Code</th>
                    <th width="7%" style=""></th>
                    <th width="11%" style=""></th>
                    <th width="22%" colspan="2" style="text-align: center">Quantity</th>
                    <th width="12%" style=""></th>
                    <th width="13%" style=""></th>
                    <th width="13%" style=""></th>
                </tr>
                <tr style="font-size:0.9em;font-weight:bold;text-align:left;overflow:hidden;">
                    <th width="4%" style="">No</th>
                    <th width="18%" style="">Barcode</th>
                    <th width="7%" style="">Loc</th>
                    <th width="11%" style="text-align:right;">Price</th>
                    <th width="11%" style="text-align:right;">Case</th>
                    <th width="11%" style="text-align:right;">Loose</th>
                    <th width="12%" style="text-align:center;">Disc %</th>
                    <th width="13%" style="text-align:right;">Ttl Disc</th>
                    <th width="13%" style="text-align:right;">Total</th>
                </tr>
            </table>
            <!-- heading -->
            </td>
            </tr>
            <?php 
            //sort details by m3, lineNo
            $tmpDetails = $docHdr->details;
            usort($tmpDetails, function ($a, $b) {
                $aItemCubicMeter = 0;
                if($a->case_decimal_qty > 0)
                {
                    $aItemCubicMeter = bcdiv($a->cubic_meter, $a->case_decimal_qty, 10);
                }
                $bItemCubicMeter = 0;
                if($b->case_decimal_qty > 0)
                {
                    $bItemCubicMeter = bcdiv($b->cubic_meter, $b->case_decimal_qty, 10);
                }
                
                if($aItemCubicMeter == $bItemCubicMeter)
                {
                    return ($a->line_no < $b->line_no) ? -1 : 1;
                }
                return ($aItemCubicMeter < $bItemCubicMeter) ? -1 : 1;
            });
            $docHdr->details = $tmpDetails;
            ?>
            <?php $startRowNum = (($pageNum - 1) * $rowsPerPage); ?>
            <?php $endRowNum = (($pageNum - 1) * $rowsPerPage) + $rowsPerPage; ?>
            @for ($rowNum = $startRowNum; $rowNum < ( $endRowNum < $ttlRows ? $endRowNum : $ttlRows ); $rowNum++)
            <tr>
            <td>
                <!-- each detail -->
                <table style="table-layout:fixed;width:191mm;font-size: 0.8em;font-weight:normal;text-align:left;overflow:hidden;">
                    <tr>
                        <!-- need this to fix the column width -->
                        <th width="4%" style="height:0.1mm;" />
                        <th width="18%" style="height:0.1mm;" />
                        <th width="7%" style="height:0.1mm;" />
                        <th width="11%" style="height:0.1mm;text-align:right;" />
                        <th width="11%" style="height:0.1mm;text-align:right;" />
                        <th width="11%" style="height:0.1mm;text-align:right;" />
                        <th width="12%" style="height:0.1mm;" />
                        <th width="13%" style="height:0.1mm;text-align:right;" />
                        <th width="13%" style="height:0.1mm;text-align:right;" />
                    </tr>
                    <tr>
                        <td width="4%" style="">{{$rowNum + 1}}</td>
                        <td width="18%" style="">{{$docHdr->details[$rowNum]['item_code']}}</td>
                        <td width="78%" colspan="7" style="">{{$docHdr->details[$rowNum]['item_desc_01']}} {{$docHdr->details[$rowNum]['item_desc_02']}}</td>
                    </tr>
                    <tr>
                        <td width="4%" style=""></td>
                        <td width="18%" style="">{{$docHdr->details[$rowNum]['item_unit_barcode']}}</td>
                        <td width="7%" style="">{{$docHdr->details[$rowNum]['location_code']}}</td>
                        <td width="11%" style="text-align:right;">
                            @if ($docHdr->details[$rowNum]['case_qty'] > 0 
                                && strcmp($docHdr->details[$rowNum]['uom_code'], $docHdr->details[$rowNum]['item_case_uom_code']) != 0)
                                {{number_format($docHdr->details[$rowNum]['case_sale_price'],2)}}
                            @else
                                {{number_format($docHdr->details[$rowNum]['sale_price'],2)}}
                            @endif
                        </td>
                        <td width="11%" style="text-align:right;">
                            @if ($docHdr->details[$rowNum]['case_qty'] > 0)
                                {{$docHdr->details[$rowNum]['case_qty']}}  {{$docHdr->details[$rowNum]['item_case_uom_code']}}
                            @endif
                        </td>
                        <td width="11%" style="text-align:right;">
                            @if ($docHdr->details[$rowNum]['loose_qty'] > 0)
                                {{number_format($docHdr->details[$rowNum]['loose_qty'],0)}}  {{$docHdr->details[$rowNum]['item_loose_uom_code']}}
                            @endif
                        </td>
                        <td width="12%" style="">
                            @if ($docHdr->details[$rowNum]['dtl_disc_amt'] > 0 && $docHdr->details[$rowNum]['net_amt'] <> 0)
                                @if ($docHdr->details[$rowNum]['dtl_disc_perc_01'] > 0)
                                    {{round($docHdr->details[$rowNum]['dtl_disc_perc_01'],2)}}%
                                @endif
                                @if ($docHdr->details[$rowNum]['dtl_disc_perc_02'] > 0)
                                    {{round($docHdr->details[$rowNum]['dtl_disc_perc_02'],2)}}%
                                @endif
                                @if ($docHdr->details[$rowNum]['dtl_disc_perc_03'] > 0)
                                    {{round($docHdr->details[$rowNum]['dtl_disc_perc_03'],2)}}%
                                @endif
                                @if ($docHdr->details[$rowNum]['dtl_disc_perc_04'] > 0)
                                    {{round($docHdr->details[$rowNum]['dtl_disc_perc_04'],2)}}% 
                                @endif
                            @endif
                            @if ($docHdr->details[$rowNum]['net_amt'] == 0)
                                FOC 
                            @endif
                        </td>
                        <td width="13%" style="text-align:right;">{{number_format($docHdr->details[$rowNum]['ttl_dtl_disc_amt'],2)}}</td>
                        <td width="13%" style="text-align:right;">{{number_format($docHdr->details[$rowNum]['net_amt'],2)}}</td>
                    </tr>
                </table>
                <!-- each detail -->
            </td>
            </tr>
            @endfor <!-- endfor : row of each item -->
            @if ($pageNum == $pageCount)
            <tr>
            <td>
                <table style="table-layout:fixed;width:191mm;border-top:1px solid black;overflow:hidden;">
                    <tr>
                        <td width="74%" style="font-size:0.8em;font-weight:bold;text-align:right">Total (RM) :</td>
                        <td width="13%" style="font-size:0.8em;font-weight:normal;text-align:right">{{number_format($docHdr->ttl_dtl_disc_amt,2)}}</td>
                        <td width="13%" style="font-size:0.8em;font-weight:normal;text-align:right">{{number_format($docHdr->net_amt,2)}}</td>
                    </tr>
                </table>
            </td>
            </tr>
            @endif
        </table>

        <!-- Print desc -->
        @if($pageNum == 1)
        <table style="top:{{$topDesc}}mm;left:34pt;position:absolute;">
            <tr>
                <td>
                    <div style="width:194mm;height:11mm;font-size:0.9em;font-weight:normal;text-align:left;overflow:hidden;">
                    {{$docHdr->desc_01}}
                    </div>
                </td>
            </tr>
        </table>
        @endif

        <!-- Print Ringgit Malaysia -->
        @if($pageNum == $pageCount)
        <table style="top:{{$topRinggit}}mm;left:12mm;position:absolute;">
            <tr>
                <td>
                    <div style="width:194mm;height:5mm;font-size:0.9em;font-weight:normal;text-align:left;overflow:hidden;">
                    Ringgit Malaysia : {{$docHdr->net_amt_english}} CENT ONLY
                    </div>
                </td>
            </tr>
        </table>
        @endif

        
        <!-- Print Invoice Barcode -->
        @if($pageNum == 1)
        <table style="top:{{$topBarcode}}mm;left:14mm;position:absolute;">
            <tr>
                <td>
                    <div style="width:141mm;height:9mm;font-size:1em;font-weight:normal;text-align:left;overflow:hidden;">
                    <?php // echo \Milon\Barcode\DNS1D::getBarcodeHTML('04'.str_pad($docHdr->id, 7, '0', STR_PAD_LEFT), "C39", 2, 15); ?>
                    </div>
                </td>
            </tr>
        </table>
        @endif

        <!-- Print Computer Signature -->
        <table style="top:{{$topSignature}}mm;left:165mm;position:absolute;">
            <tr>
                <td>
                    <div style="width:194mm;height:4mm;font-size:0.75em;font-weight:bold;text-align:left;overflow:hidden;">
                    This is computer
                    </div>
                    <div style="width:194mm;height:4mm;font-size:0.75em;font-weight:bold;text-align:left;overflow:hidden;">
                    generated invoice
                    </div>
                    <div style="width:194mm;height:4mm;font-size:0.75em;font-weight:bold;text-align:left;overflow:hidden;">
                    no signature required
                    </div>
                </td>
            </tr>
        </table>

        <!-- Print By/Time -->
        <table style="top:{{$topTime}}mm;left:152mm;position:absolute;">
            <tr>
                <td>
                    <div style="width:50mm;height:5mm;font-size:0.7em;font-weight:normal;text-align:left;overflow:hidden;">
                    {{$printedBy['username']}}   <!-- $printedAt->setTimezone(env('APP_TIMEZONE', 'UTC')) -->
                    </div>
                </td>
            </tr>
        </table>
    </div>
@endfor