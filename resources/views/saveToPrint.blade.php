<?php

?>
<html>
<head>
    <!-- Design: true will run css --->
    <!-- error when loading with wkhtmltopdf -->
    @if ($design == true) 
        <link rel="stylesheet" href="/css/style.css" />
    @endif
</head>
        <!-- load json file -->
        <!----- json file location storage/json/save.json ----->
        <?php
            $filename = 'save';
            $path = storage_path() . "/json/${filename}.json"; 
            $file = file_get_contents($path);
            $json = json_decode($file);            
        ?>

        <?php 
        ?>

        <!-- top in mm unit -->
        <?php 
            $initTopAddress = 39; 
            $initTopDocument = 49; 
            $initTopDetails = 100; 
            $initTopRinggit = 247; 
            $initTopBarcode = 255; 
            $initTopSignature = 262; 
            $initTopTime = 33; 
            $initTopDesc = 241; 
        ?>

        <!-- px value in 1 mm unit -->
        <?php $rowsPerPage = 9; ?>
        <?php $ttlRows = count($docHdr->details); ?>
        <?php $pageCount = $ttlRows % $rowsPerPage == 0 ? (int) ($ttlRows / $rowsPerPage) : (int) ($ttlRows / $rowsPerPage) + 1 ?>

@for ($pageNum = 1; $pageNum <= $pageCount; $pageNum++) 
        <?php 
            //portrait
            $loopHeight = $pageHeight;
            //use request scope variable
            $pageLoop = request()->get('slsInvBatchPrint.pageLoop', 0);

            $topAddress = $initTopAddress + $loopHeight * $pageLoop;       
            $topDocument = $initTopDocument + $loopHeight * $pageLoop;
            $topDetails = $initTopDetails + $loopHeight * $pageLoop;
            $topRinggit = $initTopRinggit + $loopHeight * $pageLoop;
            $topBarcode = $initTopBarcode + $loopHeight * $pageLoop;
            $topSignature = $initTopSignature + $loopHeight * $pageLoop;
            $topTime = $initTopTime + $loopHeight * $pageLoop;
            $topDesc = $initTopDesc + $loopHeight * $pageLoop;

            $pageLoop++;
            request()->merge(array('slsInvBatchPrint.pageLoop' => $pageLoop));
        ?> 
    <body>
    @if ($design == true) 
        <form id="myForm" action="/save" method="post">
            <input id="inputJSON" type="text" name="json" hidden>
            <input type="button" onclick="clickFunction()" value="Save">
        </form>
    @endif
    <div id="main">
    @foreach ($json->table as $tableElement)
        <!------- Loop each table starting -------->
        @if($tableElement->id == 'printDesc')
            <!-- Print desc -->
            @if($pageNum == 1)
            <table id="{{$tableElement->id}}" class="{{$tableElement->class}}" style="{{$tableElement->style}}top:{{$topDesc}}mm;">
            @endif
        @elseif($tableElement->id == 'printRM')
            <!-- Print Ringgit Malaysia -->
            @if($pageNum == $pageCount)
            <table id="{{$tableElement->id}}" class="{{$tableElement->class}}" style="{{$tableElement->style}}top:{{$topRinggit}}mm;">
            @endif
        @elseif($tableElement->id == 'printInvoiceBarcode')
            <!-- Print Invoice Barcode -->
            @if($pageNum == 1)
            <table id="{{$tableElement->id}}" class="{{$tableElement->class}}" style="{{$tableElement->style}}top:{{$topBarcode}}mm;">
            @endif
        @elseif($tableElement->id == 'printDocDetails')
            <!-- Document Details -->
            <table id="{{$tableElement->id}}" class="{{$tableElement->class}}" style="{{$tableElement->style}}top:{{$topDetails}}mm;">
        @elseif($tableElement->id == 'printSign')
            <!-- Computer Signature -->
            <table id="{{$tableElement->id}}" class="{{$tableElement->class}}" style="{{$tableElement->style}}top:{{$topSignature}}mm;">
        @elseif($tableElement->id == 'printByTime')
            <!-- Document Details -->
            <table id="{{$tableElement->id}}" class="{{$tableElement->class}}" style="{{$tableElement->style}}top:{{$topTime}}mm;">
        @elseif($tableElement->id == 'printDoc')
            <!-- Document Details -->
            <table id="{{$tableElement->id}}" class="{{$tableElement->class}}" style="{{$tableElement->style}}top:{{$topDocument}}mm;">
        @elseif($tableElement->id == 'printAdd')
            <!-- Print normal -->
            <table id="{{$tableElement->id}}" class="{{$tableElement->class}}" style="{{$tableElement->style}}top:{{$topAddress}}mm;">
        @endif
        @foreach ($tableElement->content as $tbodyElement)
            <!---- Loop Content ---->
            <!--- content need to be separate because have extra php code -->
            @if($tbodyElement->id == 'content')
                <?php 
                    //sort details by m3, lineNo
                    $tmpDetails = $docHdr->details;
                    usort($tmpDetails, function ($a, $b) {
                        $aItemCubicMeter = 0;
                        if($a->case_decimal_qty > 0)
                        {
                            $aItemCubicMeter = bcdiv($a->cubic_meter, $a->case_decimal_qty, 10);
                        }
                        $bItemCubicMeter = 0;
                        if($b->case_decimal_qty > 0)
                        {
                            $bItemCubicMeter = bcdiv($b->cubic_meter, $b->case_decimal_qty, 10);
                        }
                        
                        if($aItemCubicMeter == $bItemCubicMeter)
                        {
                            return ($a->line_no < $b->line_no) ? -1 : 1;
                        }
                        return ($aItemCubicMeter < $bItemCubicMeter) ? -1 : 1;
                    });
                    $docHdr->details = $tmpDetails;
                ?>
                <?php $startRowNum = (($pageNum - 1) * $rowsPerPage); ?>
                <?php $endRowNum = (($pageNum - 1) * $rowsPerPage) + $rowsPerPage; ?>
                @for ($rowNum = $startRowNum; $rowNum < ( $endRowNum < $ttlRows ? $endRowNum : $ttlRows ); $rowNum++)
                    <!-- each detail -->
                    <tbody id="{{$tbodyElement->id}}" class="{{$tbodyElement->class}}" style="{{$tbodyElement->style}}">
                        @foreach ($tbodyElement->content as $trElement)
                        <tr class="{{$trElement->class}}" style="{{$trElement->style}}">
                            @foreach ($trElement->content as $tdElement)
                            <td id="{{$tdElement->id}}" class="{{$tdElement->class}}" style="{{$tdElement->style}}" colspan="{{$tdElement->colspan}}" width="{{$tdElement->width}}">
                                @if($tdElement->id == 'rowNum')
                                    {{$rowNum + 1}}
                                @elseif($tdElement->id == 'itemCode')
                                    {{$docHdr->details[$rowNum]['item_code']}}
                                @elseif($tdElement->id == 'itemDesc')
                                    {{$docHdr->details[$rowNum]['item_desc_01']}}
                                    {{$docHdr->details[$rowNum]['item_desc_02']}}
                                @elseif($tdElement->id == 'itemUnitBarcode')
                                    {{$docHdr->details[$rowNum]['item_unit_barcode']}}
                                @elseif($tdElement->id == 'locationCode')
                                    {{$docHdr->details[$rowNum]['location_code']}}
                                @elseif($tdElement->id == 'Price')
                                    @if ($docHdr->details[$rowNum]['case_qty'] > 0
                                    && strcmp($docHdr->details[$rowNum]['uom_code'],
                                    $docHdr->details[$rowNum]['item_case_uom_code']) != 0)
                                        {{number_format($docHdr->details[$rowNum]['case_sale_price'],2) ?? '{CASE_SALE_PRICE}'}}
                                    @else
                                        {{number_format($docHdr->details[$rowNum]['sale_price'],2) ?? '{SALE_PRICE}'}}
                                    @endif
                                @elseif($tdElement->id == 'caseQty')
                                    @if ($docHdr->details[$rowNum]['case_qty'] > 0)
                                        {{$docHdr->details[$rowNum]['case_qty'] ?? '{CASE_QTY}'}}
                                        {{$docHdr->details[$rowNum]['item_case_uom_code'] ?? '{ITEM_CASE_UOM_CODE}'}}
                                    @endif
                                @elseif($tdElement->id == 'looseQty')
                                    @if ($docHdr->details[$rowNum]['loose_qty'] > 0)
                                        {{number_format($docHdr->details[$rowNum]['loose_qty'],0) ?? '{LOOSE_QTY}'}}
                                        {{$docHdr->details[$rowNum]['item_loose_uom_code'] ?? '{ITEM_LOOSE_UOM_CODE}'}}
                                    @endif
                                @elseif($tdElement->id == 'discount')
                                    @if ($docHdr->details[$rowNum]['dtl_disc_amt'] > 0 &&
                                        $docHdr->details[$rowNum]['net_amt'] <> 0)
                                        @if ($docHdr->details[$rowNum]['dtl_disc_perc_01'] > 0)
                                        {{round($docHdr->details[$rowNum]['dtl_disc_perc_01'],2)}}%
                                        @endif
                                        @if ($docHdr->details[$rowNum]['dtl_disc_perc_02'] > 0)
                                        {{round($docHdr->details[$rowNum]['dtl_disc_perc_02'],2)}}%
                                        @endif
                                        @if ($docHdr->details[$rowNum]['dtl_disc_perc_03'] > 0)
                                        {{round($docHdr->details[$rowNum]['dtl_disc_perc_03'],2)}}%
                                        @endif
                                        @if ($docHdr->details[$rowNum]['dtl_disc_perc_04'] > 0)
                                        {{round($docHdr->details[$rowNum]['dtl_disc_perc_04'],2)}}%
                                        @endif
                                    @endif
                                    @if ($docHdr->details[$rowNum]['net_amt'] == 0)
                                        FOC
                                    @endif
                                @elseif($tdElement->id == 'ttlDisc')
                                    {{number_format($docHdr->details[$rowNum]['ttl_dtl_disc_amt'],2) ?? '{ttl_dtl_disc_amt}'}}
                                @elseif($tdElement->id == 'netAmt')
                                    {{number_format($docHdr->details[$rowNum]['net_amt'],2)}}
                                @endif
                            </td>
                            @endforeach
                        </tr>
                        @endforeach
                    </tbody>
                <!-- each detail -->
                @endfor
                <!-- endfor : row of each item -->
                @elseif($tbodyElement->id == 'total')
                    @if ($pageNum == $pageCount)
                    <tbody id="{{$tbodyElement->id}}" class="{{$tbodyElement->class}}" style="{{$tbodyElement->style}}">
                        @foreach ($tbodyElement->content as $trElement)
                        <tr class="{{$trElement->class}}" style="{{$trElement->style}}">
                            @foreach ($trElement->content as $tdElement)
                            <td id="{{$tdElement->id}}" class="{{$tdElement->class}}" style="{{$tdElement->style}}" colspan="{{$tdElement->colspan}}" width="{{$tdElement->width}}">
                                @if($tdElement->id == 'totalText')
                                    {{$tdElement->content}}
                                @elseif($tdElement->id == 'totalDtlDisc')
                                    {{number_format($docHdr->ttl_dtl_disc_amt,2) ?? ''}}
                                @elseif($tdElement->id == 'totalNetAmt')
                                    {{number_format($docHdr->net_amt,2)}}
                                @endif
                            @endforeach
                        @endforeach
                    @endif
                @else
                <tbody id="{{$tbodyElement->id}}" class="{{$tbodyElement->class}}" style="{{$tbodyElement->style}}">
                    @foreach ($tbodyElement->content as $trElement)
                    <tr class="{{$trElement->class}}" style="{{$trElement->style}}">
                        @foreach ($trElement->content as $tdElement)
                        <td id="{{$tdElement->id}}" class="{{$tdElement->class}}" style="{{$tdElement->style}}" colspan="{{$tdElement->colspan}}" width="{{$tdElement->width}}">
                            @if(is_array($tdElement->content))
                                @foreach ($tdElement->content as $divElement)
                                    @if($divElement->id == 'billerCode')
                                    <div id="{{$divElement->id}}" style="{{$divElement->style}}">
                                        {{$docHdr->debtor_code ?? '{biller_code}'}}
                                    </div>
                                    @elseif($divElement->id == 'billerCompanyName')
                                    <div id="{{$divElement->id}}" style="{{$divElement->style}}">
                                        {{$docHdr->debtor_company_name_01 ?? '{biller_company_name}'}}
                                    </div>
                                    @elseif($divElement->id == 'billerAddress')
                                    <div id="{{$divElement->id}}" style="{{$divElement->style}}">
                                        {{$docHdr->debtor_unit_no ?? '{biller_address}'}}
                                        {{$docHdr->debtor_building_name ?? ''}}<br />
                                        {{$docHdr->debtor_street_name ?? ''}}<br />
                                        {{$docHdr->debtor_district_01 ?? ''}}<br />
                                        {{$docHdr->debtor_district_02 ?? ''}}<br />
                                        {{$docHdr->debtor_postcode ?? ''}}<br />
                                        {{$docHdr->debtor_state_name ?? ''}}
                                    </div>
                                    @elseif($divElement->id == 'billerTel')
                                    <div id="{{$divElement->id}}" style="{{$divElement->style}}">
                                        TEL : {{$docHdr->debtor_phone_01 ?? '{biller_phone}'}}
                                    </div>
                                    @elseif($divElement->id == 'billerFax')
                                    <div id="{{$divElement->id}}" style="{{$divElement->style}}">
                                        FAX : {{$docHdr->debtor_fax_01 ?? '{biller_fax}'}}
                                    </div>
                                    @elseif($divElement->id == 'deliveryAddress')
                                    <div id="{{$divElement->id}}" style="{{$divElement->style}}">
                                        {{$docHdr->delivery_point_unit_no ?? '{delivery_address}'}}
                                        {{$docHdr->delivery_point_building_name ?? ''}}<br />
                                        {{$docHdr->delivery_point_street_name ?? ''}}<br />
                                        {{$docHdr->delivery_point_district_01 ?? ''}}<br />
                                        {{$docHdr->delivery_point_district_02 ?? ''}}<br />
                                        {{$docHdr->delivery_point_postcode ?? ''}}<br />
                                        {{$docHdr->delivery_point_state_name ?? ''}}
                                    </div>
                                    @elseif($divElement->id == 'deliveryTel')
                                    <div id="{{$divElement->id}}" style="{{$divElement->style}}">
                                        TEL : {{$docHdr->delivery_point_phone_01 ?? '{delivery_phone}'}}
                                    </div>
                                    @elseif($divElement->id == 'deliveryFax')
                                    <div id="{{$divElement->id}}" style="{{$divElement->style}}">
                                        FAX : {{$docHdr->delivery_point_fax_01 ?? '{delivery_fax}'}}
                                    </div>
                                    @elseif($divElement->id == 'slsOrdHdrDocCode')
                                    <div id="{{$divElement->id}}" style="{{$divElement->style}}">
                                        {{$docHdr->sls_ord_hdr_doc_code ?? '{SO_NO}'}}
                                    </div>
                                    @elseif($divElement->id == 'pickListHdrDocCode')
                                    <div id="{{$divElement->id}}" style="{{$divElement->style}}">
                                        {{$docHdr->pick_list_hdr_doc_code ?? '{PICKING_NO}'}}
                                    </div>
                                    @elseif($divElement->id == 'invoiceNO')
                                    <div id="{{$divElement->id}}" style="{{$divElement->style}}">
                                        {{$docHdr->doc_code ?? '{INVOICE_NO}'}}
                                    </div>
                                    @elseif($divElement->id == 'docDate')
                                    <div id="{{$divElement->id}}" style="{{$divElement->style}}">
                                        {{date("Y-m-d", strtotime($docHdr->doc_date ?? '{DOC_DATE}'))}}
                                    </div>
                                    @elseif($divElement->id == 'refCode01')
                                    <div id="{{$divElement->id}}" style="{{$divElement->style}}">
                                        {{$docHdr->ref_code_01 ?? '{REF_CODE}'}}
                                    </div>
                                    @elseif($divElement->id == 'salemanCode')
                                    <div id="{{$divElement->id}}" style="{{$divElement->style}}">
                                        {{$docHdr->salesman_code ?? '{SALESMAN_CODE}'}}
                                    </div>
                                    @elseif($divElement->id == 'creditTermCode')
                                    <div id="{{$divElement->id}}" style="{{$divElement->style}}">
                                        {{$docHdr->credit_term_code ?? '{TERMS}'}}
                                    </div>
                                    @elseif($divElement->id == 'page')
                                    <div id="{{$divElement->id}}" style="{{$divElement->style}}">
                                        {{$pageNum}} / {{$pageCount ?? '{PAGE}'}}
                                    </div>
                                    @elseif($divElement->id == 'refCode02')
                                    <div id="{{$divElement->id}}" style="{{$divElement->style}}">
                                        {{$docHdr->ref_code_02 ?? '{COLLECTOR_CODE}'}}
                                    </div>
                                    @elseif($divElement->id == 'desc01')
                                    <div id="{{$divElement->id}}" style="{{$divElement->style}}">
                                        {{$docHdr->desc_01}}
                                    </div>
                                    @elseif($divElement->id == 'desc01')
                                    <div id="{{$divElement->id}}" style="{{$divElement->style}}">
                                        {{$docHdr->desc_01}}
                                    </div>
                                    @elseif($divElement->id == 'printRM')
                                    <div id="{{$divElement->id}}" style="{{$divElement->style}}">
                                        Ringgit Malaysia : {{$docHdr->net_amt_english}} CENT ONLY
                                    </div>
                                    @elseif($divElement->id == 'printBarcode')
                                    <div id="{{$divElement->id}}" style="{{$divElement->style}}">
                                        {{'barcode'}}
                                        <!-- Uncommand this when in scm backend -->
                                        <?php /* echo \Milon\Barcode\DNS1D::getBarcodeHTML('04'.str_pad($docHdr->id, 7, '0', STR_PAD_LEFT), "C39", 2, 15); */ ?>
                                    </div>
                                    @elseif($divElement->id == 'printedBy')
                                    <div id="{{$divElement->id}}" style="{{$divElement->style}}">
                                        {{$printedBy['username']}}
                                        <!-- Uncommand this and add {} to the variable when in scm backend -->
                                        <!-- $printedAt->setTimezone(env('APP_TIMEZONE', 'UTC')) -->
                                    </div>
                                    @else
                                    <div id="{{$divElement->id}}" style="{{$divElement->style}}">
                                        {{$divElement->content}}
                                    </div>
                                    @endif
                                @endforeach
                            @else
                                @php
                                    $content = $tdElement->content;
                                    echo $content;
                                @endphp
                            @endif
                        </td>
                        @endforeach
                    </tr>
                    @endforeach
                </tbody>
                @endif
            @endforeach
            </table>
            <!-- Loop each table ending -->
    @endforeach
    </div>
    <!-- End div main -->
@endfor
    </body>
</html>
<!-- Design: true will run drag and drop js --->
<!-- error when loading with wkhtmltopdf -->
@if ($design == true) 
<script type="text/javascript" src="/js/drag.js"></script>
@endif
