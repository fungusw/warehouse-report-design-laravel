<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// save the json to file controller
Route::post('/save', 'test\TestController@save');

// design with saveToPrint
Route::get('/', function () {
    // setting value for docHdr
    $docHdrs = array();
    $docHdr = new \stdClass();
    $details1 = array(
        array(
            'item_loose_uom_code' => 'LOOSE',
            'item_desc_01' => 'itemDesc1',
            'item_desc_02' => 'itemDesc2',
            'item_unit_barcode' => 'itemUnitBarcode',
            'location_code' => 'KL',
            'item_code' => 'itemCode',
            'case_decimal_qty' => 1, 
            'case_qty' => 1, 
            'uom_code' => 'UOM',
            'item_case_uom_code' => 'ITEM',
            'case_sale_price' => 1,
            'sale_price' => 12,
            'loose_qty' => 1,
            'dtl_disc_amt' => 1,
            'net_amt' => 21,
            'dtl_disc_perc_01' => 1,
            'dtl_disc_perc_02' => 0,
            'dtl_disc_perc_03' => 0,
            'dtl_disc_perc_04' => 0,
            'ttl_dtl_disc_amt' => 1,
        )
    );
    $docHdr->details = $details1;
    $docHdr->ttl_dtl_disc_amt = 1;
    $docHdr->net_amt = 2;
    $docHdr->desc_01 = '{desc-01}';
    $docHdr->net_amt_english = '{net-amt}';
    $docHdr->debtor_code = '{biller-code}';
    $docHdr->debtor_company_name_01 = '{biller-company-name}';
    $docHdr->debtor_unit_no = '{biller-unit-no}';
    $docHdr->debtor_building_name = '{biller-building-name}';
    $docHdr->debtor_street_name = '{biller-street-name}';
    $docHdr->debtor_district_01 = '{biller-district-1}';
    $docHdr->debtor_district_02 = '{biller-district-2}';
    $docHdr->debtor_district_01 = '{biller-district-1}';
    $docHdr->debtor_postcode = '{biller-postcode}';
    $docHdr->debtor_state_name = '{biller-state}';
    $docHdr->debtor_phone_01 = '{biller-phone}';
    $docHdr->debtor_fax_01 = '{biller-fax}';
    $docHdr->delivery_point_unit_no = '{delivery-unit-no}';
    $docHdr->delivery_point_building_name = '{delivery-building-name}';
    $docHdr->delivery_point_street_name = '{delivery-street-name}';
    $docHdr->delivery_point_district_01 = '{delivery-district-1}';
    $docHdr->delivery_point_district_02  = '{delivery-district-2}';
    $docHdr->delivery_point_postcode = '{delivery-postcode}';
    $docHdr->delivery_point_state_name = '{delivery-state}';
    $docHdr->delivery_point_phone_01 = '{delivery-phone}';
    $docHdr->delivery_point_fax_01 = '{delivery-fax}';
    $docHdr->sls_ord_hdr_doc_code = '{SO-NO}';
    $docHdr->pick_list_hdr_doc_code = '{PICKING-NO}';
    $docHdr->doc_code = '{INVOICE-NO}';
    // $docHdr->doc_date 
    $docHdr->ref_code_01 = '{REF-CODE}';
    $docHdr->salesman_code = '{SALESMAN-CODE}';
    $docHdr->credit_term_code = '{TERM}';
    $docHdr->ref_code_02 = '{COLLECTOR-CODE}';
    $docHdr->doc_date = '12012020';

    $docHdrs[] = $docHdr;

    

    // ------ uncomment these lines to stream pdf
    // $pdf = \Illuminate\Support\Facades\App::make('snappy.pdf.wrapper');
    // $pdf = \PDF::loadView('saveToPrint', 
    //     array(
    //         'design' => false,
    //         'docHdr' => $docHdr,
    //         // 'data' => $docHdr, 
    //         'printedAt' => '1',
    //         'printedBy' => ['username' => 'username', ],
    //         'isPageBreakAfter' => true,
    //         'pageWidth' => 300,
    //         'pageHeight' => 400
    //     )
    // );
    // $pdf->setOption('disable-smart-shrinking', true);
    // $pdf->setOption('zoom', 1.33);
    // $pdf->setOption('dpi', 300);
    // $pdf->setOption('margin-bottom', 0);
    // $pdf->setOption('margin-left', 0);
    // $pdf->setOption('margin-right', 0);
    // $pdf->setOption('margin-top', 0);
    // $pdf->setOption('page-width', '300mm');
    // $pdf->setOption('page-height', '400mm');
    // // $pdf->setOption('orientation', $printDocSetting->page_orientation);
    // return $pdf->stream();

    // ------- comment end


    return view('saveToPrint', 
    array(
        'design' => true,
        'docHdr' => $docHdr,
        'printedAt' => '1',
        'printedBy' => ['username' => 'username', ],
        'isPageBreakAfter' => true,
        'pageWidth' => 0,
        'pageHeight' => 0
    ));
});

// preview saveToPrint with wkhtmltopdf
Route::get('/preview', function() {
    $docHdrs = array();
    $docHdr = new \stdClass();
    $docHdr->details = [
        [
            'item_loose_uom_code' => 'LOOSE',
            'item_desc_01' => 'itemDesc1',
            'item_desc_02' => 'itemDesc2',
            'item_unit_barcode' => 'itemUnitBarcode',
            'location_code' => 'KL',
            'item_code' => 'itemCode',
            'case_decimal_qty' => 1, 
            'case_qty' => 1, 
            'uom_code' => 'UOM',
            'item_case_uom_code' => 'ITEM',
            'case_sale_price' => 1,
            'sale_price' => 12,
            'loose_qty' => 1,
            'dtl_disc_amt' => 1,
            'net_amt' => 21,
            'dtl_disc_perc_01' => 1,
            'dtl_disc_perc_02' => 0,
            'dtl_disc_perc_03' => 0,
            'dtl_disc_perc_04' => 0,
            'ttl_dtl_disc_amt' => 1,
        ]
    ];
    $docHdr->ttl_dtl_disc_amt = 1;
    $docHdr->net_amt = 2;
    $docHdr->desc_01 = '{desc-01}';
    $docHdr->net_amt_english = '{net-amt}';
    $docHdr->debtor_code = '{biller-code}';
    $docHdr->debtor_company_name_01 = '{biller-company-name}';
    $docHdr->debtor_unit_no = '{biller-unit-no}';
    $docHdr->debtor_building_name = '{biller-building-name}';
    $docHdr->debtor_street_name = '{biller-street-name}';
    $docHdr->debtor_district_01 = '{biller-district-1}';
    $docHdr->debtor_district_02 = '{biller-district-2}';
    $docHdr->debtor_district_01 = '{biller-district-1}';
    $docHdr->debtor_postcode = '{biller-postcode}';
    $docHdr->debtor_state_name = '{biller-state}';
    $docHdr->debtor_phone_01 = '{biller-phone}';
    $docHdr->debtor_fax_01 = '{biller-fax}';
    $docHdr->delivery_point_unit_no = '{delivery-unit-no}';
    $docHdr->delivery_point_building_name = '{delivery-building-name}';
    $docHdr->delivery_point_street_name = '{delivery-street-name}';
    $docHdr->delivery_point_district_01 = '{delivery-district-1}';
    $docHdr->delivery_point_district_02  = '{delivery-district-2}';
    $docHdr->delivery_point_postcode = '{delivery-postcode}';
    $docHdr->delivery_point_state_name = '{delivery-state}';
    $docHdr->delivery_point_phone_01 = '{delivery-phone}';
    $docHdr->delivery_point_fax_01 = '{delivery-fax}';
    $docHdr->sls_ord_hdr_doc_code = '{SO-NO}';
    $docHdr->pick_list_hdr_doc_code = '{PICKING-NO}';
    $docHdr->doc_code = '{INVOICE-NO}';
    // $docHdr->doc_date 
    $docHdr->ref_code_01 = '{REF-CODE}';
    $docHdr->salesman_code = '{SALESMAN-CODE}';
    $docHdr->credit_term_code = '{TERM}';
    $docHdr->ref_code_02 = '{COLLECTOR-CODE}';
    $docHdr->doc_date = '12012020';

    $docHdrs[] = $docHdr;

    $pdf = \Illuminate\Support\Facades\App::make('snappy.pdf.wrapper');
    $pdf = \PDF::loadView('saveToPrint', 
        array(
            'design' => false,
            'docHdr' => $docHdr,
            // 'data' => $docHdr, 
            'printedAt' => '1',
            'printedBy' => ['username' => 'username', ],
            'isPageBreakAfter' => true,
            'pageWidth' => 300,
            'pageHeight' => 400
        )
    );
    $pdf->setOption('disable-smart-shrinking', true);
    $pdf->setOption('zoom', 1.33);
    $pdf->setOption('dpi', 300);
    $pdf->setOption('margin-bottom', 0);
    $pdf->setOption('margin-left', 0);
    $pdf->setOption('margin-right', 0);
    $pdf->setOption('margin-top', 0);
    $pdf->setOption('page-width', '300mm');
    $pdf->setOption('page-height', '400mm');
    // $pdf->setOption('orientation', $printDocSetting->page_orientation);
    return $pdf->stream();
})
    
?>